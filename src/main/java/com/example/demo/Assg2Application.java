package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assg2Application {

	public static void main(String[] args) {
		SpringApplication.run(Assg2Application.class, args);
	}

}
