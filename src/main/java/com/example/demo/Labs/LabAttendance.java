package com.example.demo.Labs;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.example.demo.Users.Student;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "labattendance")
public class LabAttendance {

	@EmbeddedId
	public LabAttendanceId labAttendanceId;
	
	@Column(name = "present")
	private boolean present;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference(value = "lab-att")
	@MapsId("idlab")
	@JoinColumn(name = "idlab")
	private Lab lab;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference(value = "stud-att")
	@MapsId("idstudent")
	@JoinColumn(name = "idstudent")
	private Student student;
	
	public LabAttendance() {}

	public LabAttendance(LabAttendanceId labAttendanceId, boolean present) {
		
		super();
		this.labAttendanceId = labAttendanceId;
		this.present = present;
	}

	public Lab getLab() {
		return lab;
	}

	public void setLab(Lab lab) {
		this.lab = lab;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public LabAttendanceId getLabAttendanceId() {
		return labAttendanceId;
	}

	public void setLabAttendanceId(LabAttendanceId labAttendanceId) {
		this.labAttendanceId = labAttendanceId;
	}

	public boolean getPresent() {
		return present;
	}

	public void setPresent(boolean present) {
		this.present = present;
	}
	
}
