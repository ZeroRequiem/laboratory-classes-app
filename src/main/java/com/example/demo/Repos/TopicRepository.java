package com.example.demo.Repos;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Labs.Topic;

public interface TopicRepository extends CrudRepository<Topic, Integer>{

	public Topic findByName(String topicName);
}
