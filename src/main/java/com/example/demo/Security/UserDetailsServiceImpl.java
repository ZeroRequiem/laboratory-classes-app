package com.example.demo.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import com.example.demo.Users.Users;
import com.example.demo.Repos.*;

public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	UsersRepository usersRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Users user = usersRepository.findByUserName(username);
		
		if(user == null)
			throw new UsernameNotFoundException("could not find user with name: " + username);
		
		return new MyUserDetails(user);
	}

}
