package com.example.demo.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Repos.*;
import com.example.demo.Users.*;
import com.example.demo.Labs.*;

@Service
public class AssignmentService {

	@Autowired
	AssignmentRepository assignmentRepository;
	@Autowired
	LabAssignmentRepository labAssignmentRepository;
	
	public void addAssignment(Assignment assignment, List<Student> students) {
		
		assignment.setLabAssignments(students.stream().map(x -> {
			
			LabAssignment labAssignment = new LabAssignment();
			
			labAssignment.setLabAssignmentId(new LabAssignmentId(x.getIdStudent(), assignment.getIdAssignment()));
			labAssignment.setStudent(x);
			labAssignment.setAssignment(assignment);
			labAssignment.setContent("-");
			labAssignment.setGrade(0);
			
			return labAssignment;
		}).collect(Collectors.toSet()));
		
		assignmentRepository.save(assignment);
	}

	public List<Assignment> getAll() {
		
		List<Assignment> assignments = new ArrayList<>();
		assignmentRepository.findAll().forEach(assignments::add);
		
		return assignments;
	}
	
	public void editAssignment(Assignment assignment) {
		
		assignmentRepository.save(assignment);
	}
	
	public void deleteAssignment(int id) {
		
		assignmentRepository.deleteById(id);
	}
	
	public Assignment getAssignment(int id) {
		
		return assignmentRepository.findByIdAssignment(id);
	}
	
	public void gradeAssignment(Assignment assignment, Student student, int grade) {
		
		LabAssignment labAssignment = labAssignmentRepository.findByAssignmentIdAssignmentAndStudentIdStudent(assignment.getIdAssignment(), student.getIdStudent());
		labAssignment.setGrade(grade);
		labAssignmentRepository.save(labAssignment);
	}
	
	public void submitAssignment(LabAssignment labAssignment) {
		
		labAssignmentRepository.save(labAssignment);
	}
	
	public List<Assignment> getAllForLabId(int id){
		
		return assignmentRepository.findByLabIdLab(id);
	}
}
