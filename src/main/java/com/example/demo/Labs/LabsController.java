package com.example.demo.Labs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Users.Student;
import com.example.demo.Services.*;

@RestController
public class LabsController {
	
	@Autowired
	LabService labService;
	@Autowired
	TopicService topicService;
	@Autowired
	AssignmentService assignmentService;
	@Autowired 
	StudentService studentService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/labs/getAll")
	public List<Lab> getAllLabs(){
		
		return labService.getAllLabs();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/labs/{idLab}/attendance")
	public List<LabAttendance> getLabAttendances(@PathVariable int idLab){
		
		Lab lab = labService.getLab(idLab);
		return new ArrayList<>(lab.getLabAttendances());
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/assignment/{id}")
	public Assignment getAssignment(@PathVariable int id){
		
		return assignmentService.getAssignment(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/labs/id={id}")
	public Lab getLab(@PathVariable int id){
		
		return labService.getLab(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/teacher/topic/create")
	public void createLab(@RequestBody Topic topic){
		
		topicService.addTopic(topic);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/teacher/labs/create")
	public void createLab(@RequestBody LabForm labForm){
		
		Set<Topic> topics = topicService.getTopics(labForm.getTopics());
		labService.addLab(labForm, topics);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/teacher/labs/delete/id={id}")
	public void deleteLab(@PathVariable int id){
		
		labService.deleteLab(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/teacher/labs/update/id={id}")
	public void updateLab(@PathVariable int id, @RequestBody Lab lab){
		
		lab.setIdLab(id);
		labService.editLab(lab);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/teacher/lab/id={id}/assignment/create")
	public void createAssignment(@PathVariable int id, @RequestBody Assignment assignment){
		
		Lab lab = new Lab();
		lab.setIdLab(id);
		assignment.setLab(lab);
		
		List<Student> students = studentService.getAllStudents();
		
		assignmentService.addAssignment(assignment, students);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/teacher/labs/assignment/update/id={id}")
	public void updateAssignment(@PathVariable int id, @RequestBody Assignment assignment){
		
		assignment.setIdAssignment(id);
		assignmentService.editAssignment(assignment);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/teacher/labs/{labId}/student/{id}/attendance/{isPresent}")
	public void createLab(@PathVariable int labId, @PathVariable int id, @PathVariable boolean isPresent){
		
		LabAttendance labAttendance = new LabAttendance(new LabAttendanceId(labId, id), isPresent);
		labService.markAttendance(labAttendance);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/teacher/assignment/id={idAss}/student/id={id}/grade={grade}")
	public void gradeAssignment(@PathVariable int idAss, @PathVariable int id, @PathVariable int grade){
		
		Assignment assignment = assignmentService.getAssignment(idAss);
		Student student = studentService.getStudent(id);
		
		assignmentService.gradeAssignment(assignment, student, grade);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/student/getAllLabs")
	public List<Lab> getLabs(){
		
		List<Lab> labs = labService.getAllLabs();
		
		labs.forEach(lab -> {
			
			lab.setLabAttendances(null);
			lab.setAssignments(null);
		});
		
		return labs;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/student/getAssg/lab={id}")
	public List<Assignment> getAssignmentsForLab(@PathVariable int id){
		
		List<Assignment> assignments = assignmentService.getAllForLabId(id);
		assignments.forEach(assg -> {
			
			assg.setLabAssignments(null);
		});
		
		return assignments;
	}
}
