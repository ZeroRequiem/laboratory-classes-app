package com.example.demo.Users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Labs.*;
import com.example.demo.Services.*;

@RestController
public class UsersController {

	@Autowired
	private UsersService usersService;
	@Autowired
	private StudentService studentService;
	@Autowired
	private AssignmentService assignmentService;
	@Autowired
	private LabService labSevice;
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/users")
	public List<Users> getAllUsers(){
		
		return usersService.getAllUsers();
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "teacher/createStudent")
	@ResponseBody
	public ResponseEntity addStudent(@RequestBody Student student) {
		
		String token = TokenGenerator.getAlphaNumericString(128);
		student.setToken(token);
		
		Users user = new Users(student.getIdStudent(), TokenGenerator.getAlphaNumericString(10), TokenGenerator.getAlphaNumericString(10), false);
		usersService.addUser(user);
		
		student.setUser(user);
		List<Assignment> assignments = assignmentService.getAll();
		List<Lab> labs = labSevice.getAllLabs();
		
		studentService.addStudent(student, assignments);
		labSevice.addAttendaces(student, labs);
		
		return new ResponseEntity<>("Created student with token: " + token, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/getAllStudents")
	public List<Student> getAllStudents(){
		
		return studentService.getAllStudents();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/getStudent/email={email}")
	public Student getStudentByEmail(@PathVariable String email){
		
		return studentService.getStudentByEmail(email);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/getStudent/name={name}")
	public Student getStudentByName(@PathVariable String name){
		
		return studentService.getStudentByName(name);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/teacher/getStudent/id={id}")
	public Student getStudentByName(@PathVariable int id){
		
		return studentService.getStudent(id);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/teacher/deleteUser/id={id}")
	public void deleteStudent(@PathVariable int id){
		
		usersService.deleteUser(id);;
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/teacher/updateUser/id={id}")
	public void updateStudent(@PathVariable int id, @RequestBody Student student){
		
		usersService.updateUser(student.getUser());
		studentService.updateStudent(student);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/register")
	@ResponseBody
	public ResponseEntity registerStudent(@RequestBody RegisterForm registerForm) {
		
		Student student = studentService.getStudentByToken(registerForm.getToken());
		
		if(student != null) {
			
			Users user = student.getUser();
			user.setUserName(registerForm.getUsername());
			user.setPassword(registerForm.getPassword());
			
			usersService.updateUser(user);
			
			return new ResponseEntity<>("User info updated", HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "student/id={studId}/assignment/id={assgId}/submit")
	@ResponseBody
	public ResponseEntity submitAssignment(@RequestBody LabAssignment labAssignment, @PathVariable int studId, @PathVariable int assgId) {
		
		Student student = studentService.getStudent(studId);
		Assignment assignment = assignmentService.getAssignment(assgId);
		
		if(student != null && assignment != null) {
			
			labAssignment.setLabAssignmentId(new LabAssignmentId(student.getIdStudent(), assignment.getIdAssignment()));
			labAssignment.setAssignment(assignment);
			labAssignment.setStudent(student);
			assignmentService.submitAssignment(labAssignment);
			
			return new ResponseEntity<>("Assignment submited", HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
