package com.example.demo.Labs;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "topic")
public class Topic {

	@Id
	@Column(name = "idtopic")
	private int idTopic;
	@Column(name = "topicname")
	private String name;
	@Column(name = "description")
	private String description;
	
	@ManyToMany(mappedBy = "topics", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Lab> labs;
	
	public Topic() {}
	
	public Topic(int idTopic, Set<Lab> labs, String name, String description) {
		
		super();
		this.idTopic = idTopic;
		this.name = name;
		this.description = description;
		this.labs = labs;
	}

	public int getIdTopic() {
		return idTopic;
	}
	public void setIdTopic(int idTopic) {
		this.idTopic = idTopic;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Lab> getLabs() {
		return labs;
	}

	public void setLabs(Set<Lab> labs) {
		this.labs = labs;
	}
	
}
