package com.example.demo.Labs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators.IntSequenceGenerator;

@Entity
@Table(name = "lab")
@JsonIdentityInfo(generator = IntSequenceGenerator.class, property = "idLab")
public class Lab {

	@Id
	@Column(name = "idlab")
	private int idLab;
	@Column(name = "description")
	private String description;
	@Column(name = "date")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="yyyy-MM-dd")
	private Date date;
	@Column(name = "labname")
	private String labName;
	@OneToMany(mappedBy = "lab", cascade = CascadeType.REMOVE)
	@JsonManagedReference(value = "lab-labassg")
	private List<Assignment> assignments;
	
	@OneToMany(mappedBy = "lab", cascade = CascadeType.REMOVE)
	@JsonManagedReference(value = "lab-att")
	private Set<LabAttendance> labAttendances;
	
	@ManyToMany(cascade = CascadeType.REMOVE)
	@JoinTable(
			name = "labtopics",
			joinColumns = {@JoinColumn(name = "idlab")},
			inverseJoinColumns = {@JoinColumn(name = "idtopic")}
	)
	private Set<Topic> topics;
	
	public Lab() {};
	
	public Lab(LabForm labForm, Set<Topic> topics) {
		
		super();
		this.idLab = labForm.getId();
		this.description = labForm.getDescription();
		try {
			this.date = new SimpleDateFormat("yyyy-MM-dd").parse(labForm.getDate());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		this.topics = topics;
		this.labName = labForm.getName();
		this.topics = topics;
	}
	
	public Lab(int idLab, Set<Topic> topics, List<Assignment> assignments, String description, Date date, String name) {
		
		super();
		this.idLab = idLab;
		this.description = description;
		this.date = date;
		this.labName = name;
		this.topics = topics;
		this.assignments = assignments;
	}
	
	public int getIdLab() {
		return idLab;
	}
	public void setIdLab(int idLab) {
		this.idLab = idLab;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getName() {
		return labName;
	}
	public void setName(String name) {
		this.labName = name;
	}

	public Set<Topic> getTopics() {
		return topics;
	}

	public void setTopics(Set<Topic> topics) {
		this.topics = topics;
	}

	public String getLabName() {
		return labName;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}

	public List<Assignment> getAssignments() {
		return assignments;
	}

	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}

	public Set<LabAttendance> getLabAttendances() {
		return labAttendances;
	}

	public void setLabAttendances(Set<LabAttendance> labAttendances) {
		this.labAttendances = labAttendances;
	}
	
	
}
