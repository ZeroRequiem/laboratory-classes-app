package com.example.demo.Repos;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Users.Users;

public interface UsersRepository extends CrudRepository<Users, Integer>{

	public Users findByUserName(String username);
}
