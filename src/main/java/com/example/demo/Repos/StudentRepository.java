package com.example.demo.Repos;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Users.Student;

public interface StudentRepository extends CrudRepository<Student, Integer>{

	public Student findByEmail(String email);
	public Student findByFullName(String fullName);
	public Student findByIdStudent(int idStudent);
	public Student findByToken(String token);

}
