package com.example.demo.Labs;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "assignment")
public class Assignment {
	
	@Id
	@Column(name = "idassignment")
	private int idAssignment;
	@Column(name = "deadline")
	@Temporal(TemporalType.DATE)
    @JsonFormat(pattern="yyyy-MM-dd")
	private Date deadline;
	@Column(name = "description")
	private String description;
	@Column(name = "nameassignment")
	private String name;
	
	@ManyToOne
	@JsonBackReference(value = "lab-labassg")
	@JoinColumn(name = "idlab")
	private Lab lab;
	
	@OneToMany(mappedBy = "assignment", cascade = CascadeType.ALL)
	@JsonManagedReference(value = "lab-assg")
	private Set<LabAssignment> labAssignments;
	
	public Assignment() {}
	
	public Assignment(int idAssignment, Lab lab, Date deadline, String description, String name) {
		
		super();
		this.idAssignment = idAssignment;
		this.deadline = deadline;
		this.description = description;
		this.name = name;
		this.lab = lab;
	}
	
	public int getIdAssignment() {
		return idAssignment;
	}
	public void setIdAssignment(int idAssignment) {
		this.idAssignment = idAssignment;
	}
	public Date getDeadline() {
		return deadline;
	}
	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Lab getLab() {
		return lab;
	}

	public void setLab(Lab lab) {
		this.lab = lab;
	}

	public Set<LabAssignment> getLabAssignments() {
		return labAssignments;
	}

	public void setLabAssignments(Set<LabAssignment> labAssignments) {
		this.labAssignments = labAssignments;
	}

	
}
