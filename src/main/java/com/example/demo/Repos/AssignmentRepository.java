package com.example.demo.Repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Labs.Assignment;

public interface AssignmentRepository extends CrudRepository<Assignment, Integer>{

	public Assignment findByIdAssignment(int idAssignment);
	public List<Assignment> findByLabIdLab(int idLab);
}
