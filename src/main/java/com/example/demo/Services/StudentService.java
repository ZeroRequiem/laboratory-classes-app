package com.example.demo.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Repos.*;
import com.example.demo.Users.*;
import com.example.demo.Labs.*;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	public void addStudent(Student student, List<Assignment> assignments) {
		
		student.setLabAssignments(assignments.stream().map(x -> {
			
			LabAssignment labAssignment = new LabAssignment();
			
			labAssignment.setLabAssignmentId(new LabAssignmentId(student.getIdStudent(), x.getIdAssignment()));
			labAssignment.setStudent(student);
			labAssignment.setAssignment(x);
			labAssignment.setContent("-");
			labAssignment.setGrade(0);
			
			return labAssignment;
			
		}).collect(Collectors.toSet()));
		
		studentRepository.save(student);
	}
	
	public List<Student> getAllStudents(){
		
		List<Student> result = new ArrayList<>();
		studentRepository.findAll().forEach(result::add);
		
		return result;
	}
	
	public Student getStudentByEmail(String email) {
		
		return studentRepository.findByEmail(email);
	}
	
	public Student getStudent(int id) {
		
		return studentRepository.findByIdStudent(id);
	}
	
	public Student getStudentByName(String fullName) {
		
		return studentRepository.findByFullName(fullName);
	}
	
	public void updateStudent(Student student) {
		
		studentRepository.save(student);
	}

	public Student getStudentByToken(String token) {
		
		return studentRepository.findByToken(token);
	}
}
