package com.example.demo.Users;

public class RegisterForm extends LoginForm{

	public String token;
	
	public RegisterForm() {
		super();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
