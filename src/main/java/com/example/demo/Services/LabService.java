package com.example.demo.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Repos.*;
import com.example.demo.Users.*;
import com.example.demo.Labs.*;

@Service
public class LabService {

	@Autowired
	LabRepository labRepository;
	@Autowired
	LabAttendanceRepository labAttendanceRepository;
	
	public void addLab(LabForm labInfo, Set<Topic> topics) {
		
		Lab lab = new  Lab(labInfo, topics);
		labRepository.save(lab);
	}
	
	public void editLab(Lab lab) {
		
		labRepository.save(lab);
	}
	
	public void deleteLab(int id) {
		
		labRepository.deleteById(id);
	}

	public List<Lab> getAllLabs() {
		
		List<Lab> result = new ArrayList<>();
		labRepository.findAll().forEach(result::add);
		
		result.forEach(x -> {
		
			Set<Topic> topics = x.getTopics();
			topics.forEach(y -> y.setLabs(null));
			x.setTopics(topics);
			
		});
		
		return result;
	}
	
	public Lab getLab(int id) {
		
		return labRepository.findByIdLab(id);
	}
	
	public void markAttendance(LabAttendance labAttendance) {
		
		labAttendanceRepository.save(labAttendance);
	}

	public void addAttendaces(Student student, List<Lab> labs) {
		
		labs.forEach(x -> {
			
			LabAttendance labAttendance = new LabAttendance();
			
			labAttendance.setLabAttendanceId(new LabAttendanceId(x.getIdLab(), student.getIdStudent()));
			labAttendance.setStudent(student);
			labAttendance.setLab(x);
			labAttendance.setPresent(false);
			
			labAttendanceRepository.save(labAttendance);
		});
	}
}
