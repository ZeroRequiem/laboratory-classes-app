package com.example.demo.Repos;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Labs.LabAssignment;
import com.example.demo.Labs.LabAssignmentId;

public interface LabAssignmentRepository extends CrudRepository<LabAssignment, LabAssignmentId>{

	public LabAssignment findByAssignmentIdAssignmentAndStudentIdStudent(int idAssignment, int idStudent);
}
