package com.example.demo.Repos;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Labs.Lab;

public interface LabRepository extends CrudRepository<Lab, Integer>{

	public Lab findByIdLab(int idLab);
}
