package com.example.demo.Labs;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LabAttendanceId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "idlab")
	private int idLab;
	@Column(name = "idstudent")
	private int idStudent;
	
	public LabAttendanceId() {
	}
	
	
	
	public LabAttendanceId(int idLab, int idStudent) {
		
		super();
		this.idLab = idLab;
		this.idStudent = idStudent;
	}

	

	public int getIdLab() {
		return idLab;
	}



	public void setIdLab(int idLab) {
		this.idLab = idLab;
	}



	public int getIdStudent() {
		return idStudent;
	}



	public void setIdStudent(int idStudent) {
		this.idStudent = idStudent;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idLab;
		result = prime * result + idStudent;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LabAttendanceId other = (LabAttendanceId) obj;
		if (idLab != other.idLab)
			return false;
		if (idStudent != other.idStudent)
			return false;
		return true;
	}
	
	
}
