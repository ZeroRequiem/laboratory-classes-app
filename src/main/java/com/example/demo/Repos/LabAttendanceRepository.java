package com.example.demo.Repos;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Labs.LabAttendance;
import com.example.demo.Labs.LabAttendanceId;

public interface LabAttendanceRepository extends CrudRepository<LabAttendance, LabAttendanceId>{

}
