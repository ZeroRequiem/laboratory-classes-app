package com.example.demo.Services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Repos.*;
import com.example.demo.Labs.*;

@Service
public class TopicService {

	@Autowired
	TopicRepository topicRepository;
	
	public void addTopic(Topic topic) {
		
		topicRepository.save(topic);
	}
	
	public Set<Topic> getTopics(Set<String> topicsNames){
		
		Set<Topic> topics = new HashSet<>();
		topicsNames.forEach(x -> topics.add(topicRepository.findByName(x)));
		
		return topics;
	}
}
