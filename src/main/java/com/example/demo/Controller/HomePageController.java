package com.example.demo.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomePageController {

	@RequestMapping("/")
	public String welcomePage() {
		
		return "welcome";
	}
	
	@RequestMapping("/welcome")
	public String sayWelcome() {
		
		return "Welcome";
	}

}
