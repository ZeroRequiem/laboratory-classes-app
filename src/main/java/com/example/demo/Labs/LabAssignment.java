package com.example.demo.Labs;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.example.demo.Users.Student;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "labassignment")
public class LabAssignment {
	
	@EmbeddedId
	private LabAssignmentId labAssignmentId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference(value = "student-assg")
	@MapsId("idstudent")
	@JoinColumn(name = "idstudent")
	private Student student;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference(value = "lab-assg")
	@MapsId("idassignment")
	@JoinColumn(name = "idassignment")
	private Assignment assignment;
	
	@Column(name = "content")
	private String content;
	@Column(name = "grade")
	private int grade;
	
	public LabAssignment() {}

	public LabAssignment(LabAssignmentId labAssignmentId, Student student, Assignment assignment, String content, int grade) {
		
		this.labAssignmentId = labAssignmentId;
		this.student = student;
		this.assignment = assignment;
		this.content = content;
		this.grade = grade;
	}

	public LabAssignmentId getLabAssignmentId() {
		return labAssignmentId;
	}

	public void setLabAssignmentId(LabAssignmentId labAssignmentId) {
		this.labAssignmentId = labAssignmentId;
	}

	@JsonIgnore
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Assignment getAssignment() {
		return assignment;
	}

	public void setAssignment(Assignment assignment) {
		this.assignment = assignment;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}
	
	

}
