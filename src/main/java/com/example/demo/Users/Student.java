package com.example.demo.Users;

import java.io.Serializable;
import java.util.Set;

import org.hibernate.annotations.Parameter;

import com.example.demo.Labs.LabAssignment;
import com.example.demo.Labs.LabAttendance;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class Student {
	
	@Id
	@Column(name = "idstudent")
	private int idStudent;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idstudent")
	private Users user;
	
	@Column(name = "fullname")
	private String fullName;
	@Column(name = "email")
	private String email;
	@Column(name = "groupnr")
	private String groupNr;
	@Column(name = "hobby")
	private String  hobby;
	@Column(name = "token")
	private String token;

	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference(value = "student-assg")
	private Set<LabAssignment> labAssignments;
	
	@OneToMany(mappedBy = "student", cascade = CascadeType.REMOVE)
	@JsonManagedReference(value = "stud-att")
	private Set<LabAttendance> labAttendances;
	
	public Student() {}
	
	
	public Student(int idStudent, Set<LabAssignment> assignments, Users user, String fullName, String email, String groupNr, String hobby, String token) {
		
		super();
		this.idStudent = idStudent;
		this.user = user;
		this.email = email;
		this.groupNr = groupNr;
		this.hobby = hobby;
		this.token = token;
		this.setFullName(fullName);
		this.labAssignments = assignments;
	}
	
	public int getIdStudent() {
		return this.idStudent;
	}


	public void setIdStudent(int idStudent) {
		this.idStudent = idStudent;
	}


	public Users getUser() {
		
		return user;
	}
	
	public void setUser(Users user) {
		this.user = user;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGroupNr() {
		return groupNr;
	}
	public void setGroupNr(String groupNr) {
		this.groupNr = groupNr;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public Set<LabAssignment> getLabAssignments() {
		return labAssignments;
	}


	public void setLabAssignments(Set<LabAssignment> labAssignments) {
		this.labAssignments = labAssignments;
	}

	public Set<LabAttendance> getLabAttendances() {
		return labAttendances;
	}

	public void setLabAttendances(Set<LabAttendance> labAttendances) {
		this.labAttendances = labAttendances;
	}
}
