package com.example.demo.Labs;

import java.util.Set;

public class LabForm {

	private int id;
	private String description;
	private String date;
	private String name;
	private Set<String> topics;
	
	public LabForm() {}
	

	public LabForm(int id, String description, String date, String name, Set<String> topics) {
		
		super();
		this.id = id;
		this.description = description;
		this.date = date;
		this.name = name;
		this.topics = topics;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getTopics() {
		return topics;
	}

	public void setTopics(Set<String> topics) {
		this.topics = topics;
	}
	
}
