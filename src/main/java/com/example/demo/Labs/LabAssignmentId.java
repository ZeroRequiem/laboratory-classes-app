package com.example.demo.Labs;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LabAssignmentId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "idstudent")
	private int idStudent;
	@Column(name = "idassignment")
	private int idAssignment;
	
	public LabAssignmentId() {}

	public LabAssignmentId(int idStudent, int idAssignment) {
		this.idStudent = idStudent;
		this.idAssignment = idAssignment;
	}

	public int getIdStudent() {
		return idStudent;
	}

	public void setIdStudent(int idStudent) {
		this.idStudent = idStudent;
	}

	public int getIdAssignment() {
		return idAssignment;
	}

	public void setIdAssignment(int idAssignment) {
		this.idAssignment = idAssignment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idAssignment;
		result = prime * result + idStudent;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LabAssignmentId other = (LabAssignmentId) obj;
		if (idAssignment != other.idAssignment)
			return false;
		if (idStudent != other.idStudent)
			return false;
		return true;
	}
	
	
}
