package com.example.demo.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.Repos.*;
import com.example.demo.Users.*;

@Service
public class UsersService {

	@Autowired
	private UsersRepository usersRepository;
	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	
	public List<Users> getAllUsers() {
		
		List<Users> users = new ArrayList<>();
		usersRepository.findAll().forEach(users::add);
		
		return users;
	}

	public void addUser(Users user) {
		
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		usersRepository.save(user);
	}
	
	public void deleteUser(int id) {
	
		usersRepository.deleteById(id);
	}
	
	public void updateUser(Users user) {
		
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		usersRepository.save(user);
	}
}
